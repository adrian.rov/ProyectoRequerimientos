SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema hrdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hrdb` DEFAULT CHARACTER SET utf8 ;
USE `hrdb` ;

-- -----------------------------------------------------
-- Table `hrdb`.`sedes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`sedes` (
  `idSede` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idSede`),
  UNIQUE INDEX `idSedes_UNIQUE` (`idSede` ASC),
  UNIQUE INDEX `Nombre_UNIQUE` (`Nombre` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`imagenes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`imagenes` (
  `idImagen` INT(11) NOT NULL DEFAULT '0',
  `Nombre` VARCHAR(25) NOT NULL,
  `Tipo` VARCHAR(25) NOT NULL,
  `Imagen` BLOB NOT NULL,
  `Tamano` VARCHAR(25) NOT NULL,
  `Categoria` VARCHAR(25) NOT NULL,
  `URL` VARCHAR(2048) NULL DEFAULT NULL,
  PRIMARY KEY (`idImagen`),
  UNIQUE INDEX `idImagen_UNIQUE` (`idImagen` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`galerias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`galerias` (
  `idSede` INT(11) NOT NULL,
  `idImagen` INT(11) NOT NULL,
  PRIMARY KEY (`idSede`, `idImagen`),
  INDEX `fk_Galerias_Imagenes1_idx` (`idImagen` ASC),
  CONSTRAINT `fk_Galerias_Sedes1`
    FOREIGN KEY (`idSede`)
    REFERENCES `hrdb`.`sedes` (`idSede`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Galerias_Imagenes1`
    FOREIGN KEY (`idImagen`)
    REFERENCES `hrdb`.`imagenes` (`idImagen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`tiposdehabitacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`tiposdehabitacion` (
  `idTipodeHabitacion` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Precio` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`idTipodeHabitacion`),
  UNIQUE INDEX `idTipodeHabitacion_UNIQUE` (`idTipodeHabitacion` ASC),
  UNIQUE INDEX `Nombre_UNIQUE` (`Nombre` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`habitaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`habitaciones` (
  `idHabitacion` INT(11) NOT NULL AUTO_INCREMENT,
  `Numero` INT(11) NOT NULL,
  `TiposdeHabitacion_idTipodeHabitacion` INT(11) NOT NULL,
  PRIMARY KEY (`idHabitacion`),
  UNIQUE INDEX `idHabitacion_UNIQUE` (`idHabitacion` ASC),
  UNIQUE INDEX `Numero_UNIQUE` (`Numero` ASC),
  INDEX `fk_Habitaciones_TiposdeHabitacion1_idx` (`TiposdeHabitacion_idTipodeHabitacion` ASC),
  CONSTRAINT `fk_Habitaciones_TiposdeHabitacion1`
    FOREIGN KEY (`TiposdeHabitacion_idTipodeHabitacion`)
    REFERENCES `hrdb`.`tiposdehabitacion` (`idTipodeHabitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`paquetes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`paquetes` (
  `idPaquete` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Agotado` TINYINT(1) NOT NULL DEFAULT '0',
  `Habilitado` TINYINT(1) NOT NULL DEFAULT '1',
  `FechadeInicio` DATETIME NOT NULL,
  `FechadeFin` DATETIME NOT NULL,
  `FechadeExpiracion` DATETIME NOT NULL,
  `Descripcion` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`idPaquete`),
  UNIQUE INDEX `idPaquete_UNIQUE` (`idPaquete` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`habitacionesdepaquete`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`habitacionesdepaquete` (
  `Paquetes_idPaquete` INT(11) NOT NULL,
  `Habitaciones_idHabitacion` INT(11) NOT NULL,
  PRIMARY KEY (`Paquetes_idPaquete`, `Habitaciones_idHabitacion`),
  INDEX `fk_HabitacionesdePaquete_Habitaciones1_idx` (`Habitaciones_idHabitacion` ASC),
  CONSTRAINT `fk_HabitacionesdePaquete_Paquetes1`
    FOREIGN KEY (`Paquetes_idPaquete`)
    REFERENCES `hrdb`.`paquetes` (`idPaquete`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_HabitacionesdePaquete_Habitaciones1`
    FOREIGN KEY (`Habitaciones_idHabitacion`)
    REFERENCES `hrdb`.`habitaciones` (`idHabitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`promociones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`promociones` (
  `idPromocion` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  `FechadeInicio` DATETIME NOT NULL,
  `FechadeFin` DATETIME NOT NULL,
  `FechadeExpiracion` DATETIME NOT NULL,
  `Descripcion` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`idPromocion`),
  UNIQUE INDEX `Nombre_UNIQUE` (`Nombre` ASC),
  UNIQUE INDEX `idPromocion_UNIQUE` (`idPromocion` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`habitacionesdepromocion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`habitacionesdepromocion` (
  `idPromocion` INT(11) NOT NULL,
  `idHabitacion` INT(11) NOT NULL,
  PRIMARY KEY (`idPromocion`, `idHabitacion`),
  INDEX `fk_HabitacionesdePromocion_Habitaciones1_idx` (`idHabitacion` ASC),
  CONSTRAINT `fk_HabitacionesdePromocion_Promociones1`
    FOREIGN KEY (`idPromocion`)
    REFERENCES `hrdb`.`promociones` (`idPromocion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_HabitacionesdePromocion_Habitaciones1`
    FOREIGN KEY (`idHabitacion`)
    REFERENCES `hrdb`.`habitaciones` (`idHabitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`userprofile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`userprofile` (
  `UserId` INT(11) NOT NULL AUTO_INCREMENT,
  `UserName` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE INDEX `idUsuarios_UNIQUE` (`UserId` ASC),
  UNIQUE INDEX `Nombre_UNIQUE` (`UserName` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`tarjetas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`tarjetas` (
  `idTarjeta` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Mes` TINYINT(4) NOT NULL,
  `Ano` YEAR NOT NULL,
  `Codigo` SMALLINT(6) NOT NULL,
  `idUsuario` INT(11) NOT NULL,
  PRIMARY KEY (`idTarjeta`),
  UNIQUE INDEX `idTarjeta_UNIQUE` (`idTarjeta` ASC),
  INDEX `fk_Tarjetas_Usuarios1_idx` (`idUsuario` ASC),
  CONSTRAINT `fk_Tarjetas_Usuarios1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `hrdb`.`userprofile` (`UserId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`reservaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`reservaciones` (
  `idReservacion` INT(11) NOT NULL AUTO_INCREMENT,
  `Fecha` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idTarjeta` INT(11) NOT NULL,
  `idUsuario` INT(11) NOT NULL,
  PRIMARY KEY (`idReservacion`),
  UNIQUE INDEX `idReservacion_UNIQUE` (`idReservacion` ASC),
  INDEX `fk_Reservaciones_Tarjetas1_idx` (`idTarjeta` ASC),
  INDEX `fk_Reservaciones_Usuarios1_idx` (`idUsuario` ASC),
  CONSTRAINT `fk_Reservaciones_Tarjetas1`
    FOREIGN KEY (`idTarjeta`)
    REFERENCES `hrdb`.`tarjetas` (`idTarjeta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Reservaciones_Usuarios1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `hrdb`.`userprofile` (`UserId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`habitacionesdereservacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`habitacionesdereservacion` (
  `idReservacion` INT(11) NOT NULL,
  `idHabitacion` INT(11) NOT NULL,
  PRIMARY KEY (`idReservacion`, `idHabitacion`),
  INDEX `fk_HabitacionesdeReservacion_Habitaciones1_idx` (`idHabitacion` ASC),
  CONSTRAINT `fk_HabitacionesdeReservacion_Reservaciones1`
    FOREIGN KEY (`idReservacion`)
    REFERENCES `hrdb`.`reservaciones` (`idReservacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_HabitacionesdeReservacion_Habitaciones1`
    FOREIGN KEY (`idHabitacion`)
    REFERENCES `hrdb`.`habitaciones` (`idHabitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`habitacionesdesede`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`habitacionesdesede` (
  `idHabitacion` INT(11) NOT NULL,
  `idSede` INT(11) NOT NULL,
  PRIMARY KEY (`idHabitacion`, `idSede`),
  INDEX `fk_HabitacionesdeSede_Sedes1_idx` (`idSede` ASC),
  CONSTRAINT `fk_HabitacionesdeSede_Habitaciones1`
    FOREIGN KEY (`idHabitacion`)
    REFERENCES `hrdb`.`habitaciones` (`idHabitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_HabitacionesdeSede_Sedes1`
    FOREIGN KEY (`idSede`)
    REFERENCES `hrdb`.`sedes` (`idSede`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`noticias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`noticias` (
  `idNoticia` INT(11) NOT NULL AUTO_INCREMENT,
  `Titulo` VARCHAR(100) NOT NULL,
  `Contenido` TEXT NOT NULL,
  `Fecha` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`idNoticia`),
  UNIQUE INDEX `idNoticia_UNIQUE` (`idNoticia` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`imagenesdenoticia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`imagenesdenoticia` (
  `idImagen` INT(11) NOT NULL,
  `idNoticia` INT(11) NOT NULL,
  PRIMARY KEY (`idImagen`, `idNoticia`),
  INDEX `fk_ImagenesdeNoticia_Noticias1_idx` (`idNoticia` ASC),
  CONSTRAINT `fk_ImagenesdeNoticia_Imagenes1`
    FOREIGN KEY (`idImagen`)
    REFERENCES `hrdb`.`imagenes` (`idImagen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ImagenesdeNoticia_Noticias1`
    FOREIGN KEY (`idNoticia`)
    REFERENCES `hrdb`.`noticias` (`idNoticia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`imagenesdepromocion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`imagenesdepromocion` (
  `idPromocion` INT(11) NOT NULL,
  `idImagen` INT(11) NOT NULL,
  PRIMARY KEY (`idPromocion`, `idImagen`),
  INDEX `fk_ImagenesdePromocion_Imagenes1_idx` (`idImagen` ASC),
  CONSTRAINT `fk_ImagenesdePromocion_Promociones1`
    FOREIGN KEY (`idPromocion`)
    REFERENCES `hrdb`.`promociones` (`idPromocion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ImagenesdePromocion_Imagenes1`
    FOREIGN KEY (`idImagen`)
    REFERENCES `hrdb`.`imagenes` (`idImagen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`paises`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`paises` (
  `idPais` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idPais`),
  UNIQUE INDEX `idPaises_UNIQUE` (`idPais` ASC),
  UNIQUE INDEX `Nombre_UNIQUE` (`Nombre` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`sedesdepais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`sedesdepais` (
  `idSededePais` INT(11) NOT NULL AUTO_INCREMENT,
  `idPais` INT(11) NOT NULL,
  `idSede` INT(11) NOT NULL,
  PRIMARY KEY (`idSededePais`),
  UNIQUE INDEX `idSedesdePais_UNIQUE` (`idSededePais` ASC),
  INDEX `fk_SedesdePais_Paises1_idx` (`idPais` ASC),
  INDEX `fk_SedesdePais_Sedes1_idx` (`idSede` ASC),
  CONSTRAINT `fk_SedesdePais_Paises1`
    FOREIGN KEY (`idPais`)
    REFERENCES `hrdb`.`paises` (`idPais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SedesdePais_Sedes1`
    FOREIGN KEY (`idSede`)
    REFERENCES `hrdb`.`sedes` (`idSede`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`servicios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`servicios` (
  `idServicio` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idServicio`),
  UNIQUE INDEX `idServicio_UNIQUE` (`idServicio` ASC),
  UNIQUE INDEX `Nombre_UNIQUE` (`Nombre` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hrdb`.`serviciosdesede`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hrdb`.`serviciosdesede` (
  `idSede` INT(11) NOT NULL,
  `idServicio` INT(11) NOT NULL,
  PRIMARY KEY (`idSede`, `idServicio`),
  INDEX `fk_ServiciosdeSede_Servicios1_idx` (`idServicio` ASC),
  CONSTRAINT `fk_ServiciosdeSede_Sedes1`
    FOREIGN KEY (`idSede`)
    REFERENCES `hrdb`.`sedes` (`idSede`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ServiciosdeSede_Servicios1`
    FOREIGN KEY (`idServicio`)
    REFERENCES `hrdb`.`servicios` (`idServicio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
