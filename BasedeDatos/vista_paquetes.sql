CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `vista_paquetes` AS
    select 
        `t1`.`idPaquete` AS `ID_Paquete`,
        `t1`.`Nombre` AS `Nombre`,
        `t1`.`FechadeInicio` AS `Inicio`,
        `t1`.`FechadeFin` AS `Fin`,
        `t1`.`FechadeExpiracion` AS `Expira`,
        `t1`.`Descripcion` AS `Descripcion`,
        `t4`.`Numero` AS `#_HAB`,
        `t5`.`Nombre` AS `Tipo_HAB`
    from
        (((`paquetes` `t1`
        join `habitacionesdepaquete` `t3`)
        join `habitaciones` `t4`)
        join `tiposdehabitacion` `t5`)
    where
        ((`t3`.`Paquetes_idPaquete` = `t1`.`idPaquete`)
            and (`t3`.`Habitaciones_idHabitacion` = `t4`.`idHabitacion`)
            and (`t4`.`TiposdeHabitacion_idTipodeHabitacion` = `t5`.`idTipodeHabitacion`))