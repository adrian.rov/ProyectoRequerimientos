//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication
{
    using System;
    using System.Collections.Generic;
    
    public partial class sedesdepais
    {
        public int idSededePais { get; set; }
        public int idPais { get; set; }
        public int idSede { get; set; }
    
        public virtual paises paises { get; set; }
        public virtual sedes sedes { get; set; }
    }
}
