//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication
{
    using System;
    using System.Collections.Generic;
    
    public partial class tarjetas
    {
        public tarjetas()
        {
            this.reservaciones = new HashSet<reservaciones>();
        }
    
        public int idTarjeta { get; set; }
        public string Nombre { get; set; }
        public sbyte Mes { get; set; }
        public short Ano { get; set; }
        public short Codigo { get; set; }
        public int idUsuario { get; set; }
    
        public virtual ICollection<reservaciones> reservaciones { get; set; }
        public virtual usuarios usuarios { get; set; }
    }
}
